import React, { Component } from "react";
import { View } from "react-native";
import { array, object, string } from 'prop-types';
import { ballRadius } from '../utils/values'
export default class Box extends Component {
    render() {
        const radius = this.props.radius;
        const x = this.props.body.position.x - radius;
        const y = this.props.body.position.y - radius;
        return (
            <View
                style={{
                    position: "absolute",
                    left: x,
                    top: y,
                    width: radius * 2,
                    height: radius * 2,
                    backgroundColor: this.props.color || "pink",
                    borderRadius: ballRadius
                }} />
        );
    }
}

Box.propTypes = {
    size: array,
    body: object,
    color: string
}