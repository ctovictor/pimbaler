
import Matter from 'matter-js';
import { baseCollisionBinary, plungerBallCollisionBinary } from '../../utils/collision';
import { screenHeight, screenWidth } from '../../utils/screen';
import { frictionAir, ballRadius } from '../../utils/values';
import Box from '../Box';
import Circle from '../Circle';
const wallThickness = screenWidth / 18

const topWallBody =
    Matter.Bodies.rectangle(screenWidth / 2, wallThickness / 2,
        screenWidth, wallThickness,
        {
            isStatic: true,
            collisionFilter: { category: baseCollisionBinary, mask: baseCollisionBinary }
        });
const TopWall = {
    body: topWallBody,
    size: [screenWidth, wallThickness],
    color: 'blue',
    renderer: Box
}

const rightWallBody =
    Matter.Bodies.rectangle(
        screenWidth - wallThickness / 2, screenHeight / 2,
        wallThickness, screenHeight,
        {
            isStatic: true,
            collisionFilter: { category: baseCollisionBinary, mask: baseCollisionBinary }
        })
const RightWall = {
    body: rightWallBody,
    size: [wallThickness, screenHeight],
    color: 'blue',
    renderer: Box
}

const bottomWallBody =
    Matter.Bodies.rectangle(screenWidth / 2, screenHeight - wallThickness / 2,
        screenWidth, wallThickness,
        {
            isStatic: true,
            collisionFilter: { category: baseCollisionBinary, mask: baseCollisionBinary }
        });
const BottomWall = {
    body: bottomWallBody,
    size: [screenWidth, wallThickness],
    color: 'blue',
    renderer: Box
}

const leftWallBody =
    Matter.Bodies.rectangle(wallThickness / 2, screenHeight / 2,
        wallThickness, screenHeight,
        {
            isStatic: true,
            collisionFilter: { category: baseCollisionBinary, mask: baseCollisionBinary }
        })
const LeftWall = {
    body: leftWallBody,
    size: [wallThickness, screenHeight],
    color: 'blue',
    renderer: Box
}


const ball0Body = Matter.Bodies.circle(
    screenWidth / 2, screenHeight - wallThickness - ballRadius,
    ballRadius,
    {
        frictionAir,
        restitution: 0.99,
        collisionFilter: { category: plungerBallCollisionBinary, mask: plungerBallCollisionBinary }
    })


const ball0 = {
    body: ball0Body,
    radius: ballRadius,
    color: 'black',
    renderer: Circle
}




export default {
    TopWall, RightWall, BottomWall, LeftWall, ball0

}
