# React Native Pimbaler

This project was created as as means to learn and experience the process of using react native, expo, [react-native-game-engine](https://www.npmjs.com/package/react-native-game-engine) and [matter.js](https://brm.io/matter-js/) to model a game.

The game was inspired by the "combo-pool" game by NuSan

The project ended prematurely as it fullfilled its purpose of enlightening me not to try to make physics-based games in react native.

![](pimbalergif.gif)
