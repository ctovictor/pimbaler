import Matter from 'matter-js';
import React, { PureComponent } from "react";
import { StatusBar, StyleSheet } from 'react-native';
import { GameEngine } from "react-native-game-engine";
import Box from './Entities/Box';
import { screenWidth, screenHeight } from './utils/screen'
import { Physics, CreateBoxOnTouch, CreateCircleOnTouch, onTouch, onSwipe } from './systems'
import boardEntities from './Entities/BoardEntities'

const boxSize = Math.trunc(Math.max(screenWidth, screenHeight) * 0.075);

const floor =
  Matter.Bodies.rectangle(screenWidth / 2, screenHeight - boxSize / 2, screenWidth, boxSize, { isStatic: true });
const initialBox1Body =
  Matter.Bodies.rectangle(screenWidth / 2, screenHeight / 2, boxSize, boxSize);

const engine = Matter.Engine.create({ enableSleeping: false });
const world = engine.world;
world.gravity.y = 0

Matter.World.add(world, [...Object.keys(boardEntities).map(key => boardEntities[key].body)]);



export default class App extends PureComponent {
  render() {
    return (
      <GameEngine style={styles.container}
        systems={[Physics, onTouch, onSwipe]}
        entities={{
          physics: {
            engine,
            world
          },
          // initialBox: {
          //   body: initialBox1Body,
          //   size: [boxSize, boxSize],
          //   color: 'red',
          //   renderer: Box
          // },
          // floor: {
          //   body: floor,
          //   size: [screenWidth, boxSize],
          //   color: "green",
          //   renderer: Box
          // },
          ...boardEntities,
          plunger: {
            angle: Math.PI / 2
          },

        }}
      >
        <StatusBar hidden={true} />
      </ GameEngine >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});