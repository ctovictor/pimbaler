import { screenWidth, screenHeight } from './screen'

export const frictionAir = 0.00005 * screenWidth 
export const basePlungerForce = 0.0002 * screenWidth 
export const ballRadius = Math.trunc(Math.min(screenWidth, screenHeight) * 0.075);
