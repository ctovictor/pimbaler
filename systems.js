import Matter from 'matter-js';
import Box from './Entities/Box';
import Circle from './Entities/Circle'
import { screenWidth, screenHeight } from './utils/screen'
import { baseCollisionBinary, plungerBallCollisionBinary } from './utils/collision'
import { frictionAir, basePlungerForce, ballRadius } from './utils/values'
let boxIds = 0;
export const CreateBoxOnTouch = (entities, { touches, screen }) => {
  let world = entities["physics"].world;
  let boxSize = Math.trunc(Math.max(screen.width, screen.height) * 0.075);
  touches.filter(t => t.type === "press").forEach(t => {
    console.log(entities)
    let body = Matter.Bodies.rectangle(
      t.event.pageX, t.event.pageY,
      boxSize, boxSize,
      { frictionAir: 0.021, restitution: 0.5 });
    Matter.World.add(world, [body]);
    entities[`box${++boxIds}`] = {
      body: body,
      size: [boxSize, boxSize],
      color: boxIds % 2 == 0 ? "pink" : "#B8E986",
      renderer: Box
    };
  });
  return entities;
};

let currentCircleId = 0;
export const CreateCircleOnTouch = (entities, { touches, screen }) => {
  let world = entities["physics"].world;
  let radius = Math.trunc(Math.min(screen.width, screen.height) * 0.075);
  touches.filter(t => t.type === "press").forEach(t => {
    console.log(entities)
    let body = Matter.Bodies.circle(
      t.event.pageX, t.event.pageY,
      radius,
      { frictionAir: 0.021, restitution: 0.4 });
    Matter.World.add(world, [body]);
    entities[`circle${++currentCircleId}`] = {
      body: body,
      radius,
      color: boxIds % 2 == 0 ? "pink" : "#B8E986",
      renderer: Circle
    };
  });
  return entities;
};

let ballId = 0
export const onTouch = (entities, { touches, screen }) => {
  let world = entities["physics"].world;

  touches.filter(t => t.type === "press").forEach(t => {
    console.log(entities)
    const ballBody = entities[`ball${ballId}`].body
    const plunger = entities['plunger']
    const angle = plunger.angle
    ballBody.collisionFilter.category = baseCollisionBinary
    ballBody.collisionFilter.mask = baseCollisionBinary
    Matter.Body.applyForce(ballBody,
      { x: ballBody.position.x, y: ballBody.position.y },
      { x: Math.cos(angle) * basePlungerForce * 0.1, y: Math.sin(angle) * -basePlungerForce * 0.1 }
    )
    const wallThickness = screenWidth / 18

    let newCircleBody = Matter.Bodies.circle(
      screenWidth / 2, screenHeight - wallThickness - ballRadius,
      ballRadius,
      { frictionAir, restitution: 0.99, collisionFilter: { category: plungerBallCollisionBinary, mask: plungerBallCollisionBinary } })

    Matter.World.add(world, [newCircleBody]);
    entities[`ball${++ballId}`] = {
      body: newCircleBody,
      radius: ballRadius,
      color: boxIds % 2 == 0 ? "pink" : "#B8E986",
      renderer: Circle
    };

  });
  return entities
}

export const onSwipe = (entities, { touches, screen }) => {
  touches.filter(t => t.type === "move").forEach(t => {
    const plunger = entities['plunger']

    plunger.angle = plunger.angle - t.delta.pageX * 0.01
    if (plunger.angle > Math.PI) plunger.angle = Math.PI
    if (plunger.angle < 0) plunger.angle = 0
    console.log(plunger)

  })
  return entities
}

export const Physics = (entities, { time }) => {
  let engine = entities["physics"].engine;
  Matter.Engine.update(engine, time.delta);
  return entities;
};